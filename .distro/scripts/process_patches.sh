TARBALL=$1
shift
SPECNAME=$1
shift
MARKER=$1
shift
LOCALVERSION=$1
shift

SCRIPTS=scripts
SOURCES=rpmbuild/SOURCES
SRPMDIR=rpmbuild/SRPM
SPEC=rpmbuild/SPECS/${SPECNAME}

LOCAL_PYTHON=python3

# Pre-cleaning
rm -rf .tmp asection psection patchlist

cp ${TARBALL} ${SOURCES}/${TARBALL}

# Handle patches
git format-patch --first-parent --no-cover-letter --no-renames -k --no-binary ${MARKER}.. > patchlist
num=1
for patchfile in `cat patchlist`; do
  ${LOCAL_PYTHON} ${SCRIPTS}/frh.py ${patchfile} > .tmp
  if grep -q '^diff --git ' .tmp; then
    patchname=$(grep -x "Patch-name: .*\.patch" .tmp | sed 's/Patch-name: \(.*\)/\1/')
    if [ -z "$patchname" ]; then
        patchname=$(grep -x "patch_name: .*\.patch" .tmp | sed 's/patch_name: \(.*\)/\1/')
    fi
    patchid=$(grep -x "Patch-id: .*" .tmp | sed 's/Patch-id: \(.*\)/\1/')
    inspec=$(grep -x "Patch-present-in-specfile: .*" .tmp | sed 's/Patch-present-in-specfile: \(.*\)/\1/')
    if [ -z "$inspec" ]; then
        inspec=$(grep -x "present_in_specfile: .*" .tmp | sed 's/present_in_specfile: \(.*\)/\1/')
    fi
    if [ -z "$inspec" ]; then
	    if [ -n "$(grep From-dist-git-commit .tmp)" ]; then
                inspec="True"
	    fi
    fi
    ignore=$(grep -x "Ignore-patch: .*" .tmp | sed 's/Ignore-patch: \(.*\)/\1/')

    if [ "$ignore" == "True" ]; then
        continue
    fi

    if [ -n "$patchname" ]; then
        mv .tmp ${SOURCES}/${patchname}
    else
        mv .tmp ${SOURCES}/${patchfile}
    fi
    if [ -n "$patchid" ]; then
       if [ "$patchid" -gt "$num" ]; then
	   num=$patchid
       fi
    else
	let num=num+1
    fi
    if [ "$inspec" != "True" -a "$inspec" != "true" ]; then
        echo "Patch${num}: ${patchfile}" >> psection
        echo "%patch${num} -p2" >> asection
    fi
  fi
done

cp ${SPECNAME} ${SPEC}
sed -i '/# Source-git patches/r psection' ${SPEC}
sed -i '/# Apply source-git patches/r asection' ${SPEC}
if [ -n "${LOCALVERSION}" ]; then
  sed -i "s/\(Release: .*%{?dist}\)/\1\.${LOCALVERSION}/" ${SPEC}
fi

# Post-cleaning
rm -rf $(cat patchlist)
rm -rf .tmp asection psection patchlist
